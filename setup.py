from os.path import join
from setuptools import (setup,
                        find_packages)

packages = find_packages(exclude=['tests*'])
with open('requirements.txt') as f:
    requirements = f.read().split()

with open('README.md') as f:
    long_description = f.read()

filename = join('twigraf', 'version.py')
with open(filename) as f:
    exec(compile(f.read(), filename, 'exec'))

setup(name='twigraf',
      version=__version__,
      url='https://gitlab.com/AlirezaH320/twigraf',
      author='Alireza Hosseini',
      author_email='alirezah320@yahoo.com',
      packages=packages,
      include_package_data=True,
      package_data={'twigraf.twitter': ['creds.json']},
      description='A Twitter Telegram forwarder',
      long_description=long_description,
      long_description_content_type="text/markdown",
      install_requires=requirements,
      entry_points={'console_scripts': ['twigraf = twigraf:main']})
