import pytest

from environs import Env

from twigraf.db.base import SessionFactory
from twigraf.twitter.bots import DirectMessageBot
from twigraf.telegram.types import NetworkUsageLevel


@pytest.fixture(scope='session')
def admin_user_id():
    return 992379951918845958


@pytest.fixture(scope='session')
def dm_bot():
    env = Env()
    env.read_env('environments/debug.env', recurse=False)
    session_factory = SessionFactory(env('database_engine'),
                                     env('database_user'),
                                     env('database_password'),
                                     env('database_host'),
                                     env('database_port'),
                                     env('database_name'),
                                     False)
    session = session_factory.new_session()
    bot = DirectMessageBot(env('twitter_platform_to_use'),
                           env('telegram_channel_username'),
                           env('telegram_media_channel_username'),
                           env('telegram_log_channel_username'),
                           env.int('telegram_api_id'),
                           env('telegram_api_hash'),
                           env('telegram_phone'),
                           env('telegram_password'),
                           env('telegram_bot_token'),
                           env('latest_admins_screen_names'),
                           NetworkUsageLevel.high,
                           session)
    return bot
