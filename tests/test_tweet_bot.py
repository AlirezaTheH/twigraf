class TestTweetBot:
    def test_send_text_only_tweet(self, dm_bot, admin_user_id):
        status_id = 1282026164714905602
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_reply_tweet(self, dm_bot, admin_user_id):
        status_id = 1272957954401226756
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_simple_quote_tweet(self, dm_bot, admin_user_id):
        status_id = 1282987009502584832
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_link_quote_tweet(self, dm_bot, admin_user_id):
        status_id = 1282226474162978822
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_single_photo_tweet(self, dm_bot, admin_user_id):
        status_id = 1282813337718464517
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_multiple_photo_tweet(self, dm_bot, admin_user_id):
        status_id = 1282995782442876928
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_gif_tweet(self, dm_bot, admin_user_id):
        status_id = 1282592949314912256
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_video_tweet(self, dm_bot, admin_user_id):
        status_id = 1282059863976288256
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_long_video_tweet(self, dm_bot, admin_user_id):
        status_id = 1058708775144697856
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_thread_tweet(self, dm_bot, admin_user_id):
        status_id = 1275517060567699465
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_middle_thread_tweet(self, dm_bot, admin_user_id):
        status_id = 1275517073939185665
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_poll_text_only_tweet(self, dm_bot, admin_user_id):
        status_id = 1281280435436359680
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_poll_photo_tweet(self, dm_bot, admin_user_id):
        status_id = 1085337686133530624
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_poll_video_tweet(self, dm_bot, admin_user_id):
        status_id = 1057869109214334977
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_summary_tweet(self, dm_bot, admin_user_id):
        status_id = 1280363266774978560
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_summary_large_image_tweet(self, dm_bot, admin_user_id):
        status_id = 1281566338960568321
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_promo_website_tweet(self, dm_bot, admin_user_id):
        status_id = 1280180960533680130
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_promo_video_website_tweet(self, dm_bot, admin_user_id):
        status_id = 1281302716527108102
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_periscope_broadcast_tweet(self, dm_bot, admin_user_id):
        status_id = 1281310948750503936
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_broadcast_tweet(self, dm_bot, admin_user_id):
        status_id = 1280943561031274496
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_live_event_tweet(self, dm_bot, admin_user_id):
        status_id = 975754783918297094
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_amplify_tweet(self, dm_bot, admin_user_id):
        status_id = 736849927314284545
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None

    def test_send_player_tweet(self, dm_bot, admin_user_id):
        status_id = 1290643005490311171
        assert dm_bot.parse_and_send(status_id, status_id, admin_user_id) is not None
