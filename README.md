# Twigraf (Twitter Telegram Forwarder)
`twigraf` is a [Telegram](https://telegram.org) bot that forwards a [Twitter](https://twitter.com) account's tweets to a Telegram channel in a decent way.

## Installation
To pip install `twigraf` from gitlab:
```bash
pip install git+https://gitlab.com/AlirezaH320/twigraf.git
```
