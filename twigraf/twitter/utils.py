from datetime import datetime
from email.utils import parsedate


def parse_datetime(string):
    """
    Parses a string datetime

    Parameters
    ----------
    string: str
        The string datetime

    Returns
    -------
    result: datetime
        The parsed datetime
    """
    return datetime(*(parsedate(string)[:6]))
