import time
import asyncio
import re
from datetime import datetime

from telethon import TelegramClient
from telethon.utils import get_peer_id
from sqlalchemy.orm import Session

from twigraf.db.models import (Configuration,
                               TwitterCredential,
                               TwitterUser,
                               TwitterUserPublishedTweet,
                               TweetThread)
from twigraf.db.queries import (get_configuration,
                                get_tweet,
                                get_twitter_user,
                                get_twitter_admin_users)
from twigraf.db.parser import new_tweet_from_json
from twigraf.db.types import TwitterUserType
from twigraf.twitter.api import Api
from twigraf.twitter.decorators import handle_db_disconnection
from twigraf.telegram.bots import TweetBot


class DirectMessageBot:
    """
    A bot that listens on tweets sent from admins and save them on the db,
    then sends them to connected telegram channel.

    Attributes
    ----------
    session: Session
        The db session

    configuration: Configuration
        The configuration to use dm bot, telegram and api

    api: Api
        The api object that retrieve twitter data

    tweet_bot: TweetBot
        the telegram bot that sends tweets to channel

    admins: dict
        Dict of admins user ids to admins
    """

    def __init__(self,
                 platform,
                 telegram_channel_username,
                 telegram_media_channel_username,
                 telegram_log_channel_username,
                 telegram_api_id,
                 telegram_api_hash,
                 telegram_phone,
                 telegram_password,
                 telegram_bot_token,
                 latest_admins_screen_names,
                 network_usage_level,
                 session):
        """
        Initiates the listener

        Parameters
        ---------
        platform: str
            Platform that will authenticate twitter account with

        telegram_channel_username: str
            The telegram channel that will be connected to account

        telegram_media_channel_username: str
            The media telegram channel that will be connected to account

        telegram_log_channel_username: str
            The log telegram channel that will be connected to account

        telegram_api_id: int
            Telegram api id

        telegram_api_hash: str
            Telegram api hash

        telegram_phone: str
            Telegram phone

        telegram_password: str
            Telegram password

        telegram_bot_token: str
            Telegram telegram's unique authentication

        latest_admins_screen_names: list
            List of latest admins screen names

        network_usage_level: str
            Indicates network usage level. see NetworkUsageLevel for available levels.

        session: Session
            The db session
        """
        self.session = session
        loop = asyncio.get_event_loop()
        self.api, self.configuration = loop.run_until_complete(self.connect(platform,
                                                                            telegram_channel_username,
                                                                            telegram_media_channel_username,
                                                                            telegram_log_channel_username,
                                                                            telegram_api_id,
                                                                            telegram_api_hash,
                                                                            telegram_bot_token))
        self.tweet_bot = TweetBot(telegram_api_id,
                                  telegram_api_hash,
                                  telegram_phone,
                                  telegram_password,
                                  telegram_bot_token,
                                  self.configuration,
                                  network_usage_level)
        self.update_admins(latest_admins_screen_names)
        self.admins = {admin.id: admin for admin in get_twitter_admin_users(self.session)}

    async def connect(self,
                      platform,
                      telegram_channel_username,
                      telegram_media_channel_username,
                      telegram_log_channel_username,
                      telegram_api_id,
                      telegram_api_hash,
                      telegram_bot_token):
        """
        Connects to twitter api and db.

        Parameters
        ----------
        platform: str
            Platform that will authenticate twitter account with

        telegram_channel_username: str
            The telegram channel that will be connected to account

        telegram_media_channel_username: str
            The media telegram channel that will be connected to account

        telegram_log_channel_username: str
            The log telegram channel that will be connected to account

        telegram_api_id: int
            Telegram api id

        telegram_api_hash: str
            Telegram api hash

        telegram_bot_token: str
            Telegram telegram's unique authentication

        Returns
        -------
        result: tuple
            Tuple of api and twitter account
        """
        configuration = get_configuration(self.session)
        if configuration is None:
            api = Api(platform=platform)
            user = api.me()
            bot = TelegramClient('bot', telegram_api_id, telegram_api_hash).start(bot_token=telegram_bot_token)
            channel = await bot.get_entity(telegram_channel_username)
            media_channel = await bot.get_entity(telegram_media_channel_username)
            log_channel = await bot.get_entity(telegram_log_channel_username)
            bot.disconnect()

            credential = TwitterCredential(platform=platform,
                                           access_token=api.access_token,
                                           access_token_secret=api.access_token_secret)
            configuration = Configuration(twitter_account_id=user['id'],
                                          twitter_account_name=user['name'],
                                          twitter_account_screen_name=user['screen_name'],
                                          telegram_channel_id=get_peer_id(channel),
                                          telegram_channel_username=telegram_channel_username,
                                          telegram_media_channel_id=get_peer_id(media_channel),
                                          telegram_media_channel_username=telegram_media_channel_username,
                                          telegram_log_channel_id=get_peer_id(log_channel),
                                          telegram_log_channel_username=telegram_log_channel_username,
                                          credentials=[credential])
            self.session.add(configuration)
        else:
            credential = configuration.get_twitter_credential(platform)
            if credential is None:
                api = Api(platform=platform)
                credential = TwitterCredential(platform=platform,
                                               access_token=api.access_token,
                                               access_token_secret=api.access_token_secret)
                configuration.credentials.append(credential)
            else:
                api = Api(access_token=credential.access_token,
                          access_token_secret=credential.access_token_secret,
                          platform=platform)

            user = api.me()
            configuration.twitter_account_name = user['name']
            configuration.twitter_account_screen_name = user['screen_name']

        self.session.commit()
        return api, configuration

    def update_admins(self, latest_admins_screen_names):
        """
        Updates bot admins.

        Parameters
        ----------
        latest_admins_screen_names: list
            List of latest admins screen names
        """
        latest_admins = {admin['id']: admin
                         for admin in self.api.user_lookup(latest_admins_screen_names,
                                                           id_type='screen_name')}
        current_admins = {admin.id: admin for admin in get_twitter_admin_users(self.session)}

        latest_admins_ids = set(latest_admins)
        current_admins_ids = set(current_admins)

        remained_admins_ids = current_admins_ids & latest_admins_ids
        fired_admins_ids = current_admins_ids - latest_admins_ids
        new_admins_ids = latest_admins_ids - current_admins_ids

        for admin_id in remained_admins_ids:
            latest_admin = latest_admins[admin_id]
            current_admin = current_admins[admin_id]
            current_admin.name = latest_admin['name']
            current_admin.screen_name = latest_admin['screen_name']

        for admin_id in fired_admins_ids:
            current_admin = current_admins[admin_id]
            current_admin.type = TwitterUserType.user

        for admin_id in new_admins_ids:
            latest_admin = latest_admins[admin_id]
            twitter_user = get_twitter_user(admin_id, self.session)
            if twitter_user is None:
                twitter_user = TwitterUser(id=latest_admin['id'],
                                           name=latest_admin['name'],
                                           screen_name=latest_admin['screen_name'],
                                           type=TwitterUserType.admin)
                self.session.add(twitter_user)
            else:
                twitter_user.name = latest_admin['name']
                twitter_user.screen_name = latest_admin['screen_name']
                twitter_user.type = TwitterUserType.admin

        self.session.commit()

    def listen(self):
        """
        Start listening.
        """
        me = self.api.me()
        cursor = self.api.get_inbox_cursor()
        status_url_pattern = re.compile(r'https://twitter\.com/(?P<screen_name>\w+)/status/\d+')
        while True:
            cursor, new_messages = self.api.get_direct_message_updates(cursor)
            for message in new_messages:
                message_data = message['message_data']

                if int(message_data['sender_id']) in self.admins:
                    status = message_data.get('attachment', {}).get('tweet', {}).get('status', None)
                    status_url = message_data.get('entities', {}).get('urls', [{}])[-1].get('expanded_url', '')
                    status_url_match = re.search(status_url_pattern, status_url)

                    if status is None and status_url_match is not None:
                        user = self.api.get_user(status_url_match.group('screen_name'), id_type='screen_name')
                        if user['protected']:
                            self.api.send_direct_message('توییت‌های این کاربر محافظت شده.', message['conversation_id'])

                        elif user['blocked_by']:
                            self.api.send_direct_message('این کاربر ما رو مسدود کرده.', message['conversation_id'])

                        elif user['blocking']:
                            self.api.send_direct_message('این کاربر توسط ما مسدود شده.', message['conversation_id'])

                    elif status is not None:
                        if status['retweeted']:
                            self.api.send_direct_message('این توییت قبلاً بازنشر شده.', message['conversation_id'])
                        else:
                            retweeted_status = self.api.retweet(status['id'])
                            self.parse_and_send(status['id'],
                                                retweeted_status['id'],
                                                int(message_data['sender_id']))
                            self.api.send_direct_message('بازنشرش دادم.', message['conversation_id'])
                    else:
                        self.api.send_direct_message('توییتی برای بازنشر وجود نداره.', message['conversation_id'])

                elif message_data['sender_id'] != me['id_str']:
                    self.api.send_direct_message('سلام\nلطفاً برای ارتباط با ما از بات تلگراممون استفاده کن. '
                                                 'لینکش تو بیوی توییترمون هست.',
                                                 message['conversation_id'])
            time.sleep(5)

    @handle_db_disconnection
    def parse_and_send(self, status_id, url_tweet_id, admin_user_id, creation_date=datetime.now()):
        """
        Parses conversation statuses of a status and save them on the db if don't exist,
        then send them to telegram channel.

        Parameters
        ----------
        status_id: int
            The status id

        url_tweet_id: int
            The tweet id that will be used in telegram's sent message's text

        admin_user_id: int
            The publisher admin user id

        creation_date: datetime
            The creation date of published status

        Returns
        -------
        message: telegram.Message
            On success, the sent message is returned.
        """
        tweet = get_tweet(status_id, self.session)

        # If tweet does not exists on db
        if tweet is None:
            conversation_statuses = self.api.get_conversation_statuses(status_id)
            if len(conversation_statuses) == 1:
                tweet = new_tweet_from_json(conversation_statuses[0], self.session)
                message = self.tweet_bot.send_tweet(tweet, url_tweet_id)
            else:
                tweet_thread = TweetThread(id=status_id)
                conversation_tweets = [new_tweet_from_json(conversation_statuses[0], self.session, tweet_thread)]
                for s in conversation_statuses[1:]:
                    tweet = get_tweet(s['id'], self.session)
                    if tweet is None:
                        tweet = new_tweet_from_json(s, self.session, tweet_thread)
                    else:
                        tweet.tweet_thread = tweet_thread
                    conversation_tweets.append(tweet)
                tweet = conversation_tweets[0]
                message = self.tweet_bot.send_thread(conversation_tweets, url_tweet_id)

        # If tweet exists on db
        else:
            # tweet is a thread root
            if tweet.thread_tweet_id == tweet.id:
                tweets = tweet.tweet_thread.tweets
                message = self.tweet_bot.send_thread(tweets, url_tweet_id)
            else:
                message = self.tweet_bot.send_tweet(tweet, url_tweet_id)

        published_tweet = TwitterUserPublishedTweet(url_tweet_id=url_tweet_id,
                                                    tweet=tweet,
                                                    creation_date=creation_date)
        admin = self.admins[admin_user_id]
        admin.published_tweets.append(published_tweet)
        self.session.commit()
        return message
