import time
import logging

from sqlalchemy import exc
from requests.exceptions import ReadTimeout
from urllib3.exceptions import ReadTimeoutError

logger = logging.getLogger('twigraf')


def handle_db_disconnection(function):
    """
    A decorator to handle unexpected db errors.
    """
    def new_function(self, *args, **kwargs):
        try:
            return function(self, *args, **kwargs)
        except exc.DBAPIError as e:
            if e.connection_invalidated:
                logger.warning('Database connection has invalidated!')
                self.session.rollback()
                return new_function(self, *args, **kwargs)
            else:
                raise e

    return new_function


def handle_rate_limit(function):
    """
    A decorator to handle rate limiting from the Twitter API. If
    a rate limit error is encountered we will sleep until we can
    issue the API call again.
    """
    def new_function(*args, **kwargs):
        response = function(*args, **kwargs)
        if response.status_code == 429:
            reset = int(response.headers['x-rate-limit-reset'])
            now = time.time()
            seconds = reset - now + 10
            if seconds < 1:
                seconds = 10
            logger.warning(f'Rate limit exceeded: sleeping {round(seconds)} seconds.')
            time.sleep(seconds)
            return new_function(*args, **kwargs)

        return response

    return new_function


def handle_timeout(function):
    """
    A decorator to handle read timeouts from Twitter.
    """
    def new_function(self, *args, **kwargs):
        try:
            return function(self, *args, **kwargs)
        except (ReadTimeout, ReadTimeoutError) as e:
            logger.warning('Caught read timeout')
            self.connect()
            return function(self, *args, **kwargs)
    return new_function