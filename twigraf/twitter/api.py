import time
import re
import ssl
import json
from urllib.parse import parse_qs
from os.path import (join,
                     dirname)

import requests
from requests.exceptions import ConnectionError
from urllib3.exceptions import ProtocolError
from requests_oauthlib import (OAuth1,
                               OAuth1Session)

from twigraf.twitter.decorators import (handle_rate_limit,
                                        handle_timeout)


class Api:
    """
    Provides super fast methods for retrieving data from twitter.
    """
    def __init__(self,
                 consumer_key=None,
                 consumer_secret=None,
                 access_token=None,
                 access_token_secret=None,
                 platform=None):
        """
        Initiates the api

        Parameters
        ----------
        consumer_key: str
            Twitter app consumer key if have one

        consumer_secret: str
            Twitter app consumer secret if have one

        access_token: str
            Account access token if have one

        access_token_secret: str
            Account access token secret if have one

        platform: str
            Platform that will authenticate twitter account with in case you don't have
            consumer key and secret
        """
        self.consumer_key = consumer_key
        self.consumer_secret = consumer_secret
        self.access_token = access_token
        self.access_token_secret = access_token_secret

        if self.consumer_key is None or self.consumer_secret is None:
            if platform is None:
                raise ValueError('You must either provide platform or tuple of consumer_key and consumer_secret.')
            else:
                with open(join(dirname(__file__), 'creds.json')) as f:
                    data = json.load(f)
                platform_credentials = data[platform]
                self.consumer_key = platform_credentials['consumer_key']
                self.consumer_secret = platform_credentials['consumer_secret']

        if self.access_token is None or self.access_token_secret is None:
            self.configure()

        self.client = None

    def me(self):
        """
        Gets the api authenticated user.

        Returns
        -------
        user: dict
            The user json
        """
        url = 'https://api.twitter.com/1.1/account/verify_credentials.json'
        user = self.get(url, allow_404=True).json()
        return user

    def mark_read_direct_messages(self, last_read_event_id):
        url = 'https://api.twitter.com/1.1/dm/conversation/mark_read.json'
        params = {'last_read_event_id': last_read_event_id}
        self.post(url, params=params)

    def get_inbox_cursor(self):
        url = 'https://api.twitter.com/1.1/dm/user_updates.json'
        response = self.get(url, allow_404=True).json()
        inbox_cursor = response['user_inbox']['cursor']
        return inbox_cursor

    def get_direct_message_updates(self, cursor):
        url = 'https://api.twitter.com/1.1/dm/user_updates.json'
        params = {'cursor': cursor}
        response = self.get(url, params=params, allow_404=True).json()
        user_events = response['user_events']
        cursor = user_events['cursor']
        new_messages = []
        if 'entries' in user_events:
            entries = user_events['entries']

            for entry in entries:
                if 'message' in entry:
                    new_messages.append(entry['message'])

            self.mark_read_direct_messages(user_events['max_entry_id'])

        return cursor, new_messages

    def send_direct_message(self, text, conversation_id):
        url = 'https://api.twitter.com/1.1/dm/new.json'
        params = {'text': text,
                  'conversation_id': conversation_id}
        self.post(url, params=params)

    def get_conversation_statuses(self, conversation_id):
        """
        Gets all statuses in a conversation

        Parameters
        ----------
        conversation_id: int
            The conversation id which is a status id

        Returns
        -------
        conversation_statuses: list
            List of conversation statuses
        """

        def resolve_quote(s):
            s['quoted_user'] = users[statuses[s['quoted_status_id_str']]['user_id_str']]

            # If quote is not a simple quote remove the extra link
            permalink_url = s['quoted_status_permalink']['url']
            permalink_pattern = re.compile(rf'{re.escape(permalink_url)}$')
            status_text = s['full_text']

            if re.search(permalink_pattern, status_text):
                s['full_text'] = re.sub(permalink_pattern, '', status_text)
                s['entities']['urls'] = s['entities']['urls'][:-1]

        def resolve_status_urls(s):
            status_url_pattern = re.compile(r'https://twitter\.com/(?P<screen_name>\w+)/status/\d+')
            url_users = {}
            urls = s['entities']['urls']

            for i, u in enumerate(urls):
                status_url_match = re.search(status_url_pattern, u['expanded_url'])

                if status_url_match:
                    u['is_status_url'] = True
                    u['expanded_url'] = status_url_match.group()
                    if s['quoted_status_permalink']['url'] == u['url']:
                        u['user'] = s['quoted_user']

                    else:
                        url_users[i] = status_url_match.group('screen_name')
                else:
                    u['is_status_url'] = False

            if len(url_users) > 0:
                for i, u in zip(url_users, self.user_lookup(url_users.values(), 'screen_name')):
                    urls[i]['status_user'] = u

        url = f'https://api.twitter.com/2/timeline/conversation/{conversation_id}.json'
        params = {'count': 1,
                  'include_cards': True,
                  'cards_platform': 'Web-12'}
        response = self.get(url, params=params).json()
        statuses = response['globalObjects']['tweets']
        users = response['globalObjects']['users']

        status = statuses[str(conversation_id)]
        user = status['user'] = users[status['user_id_str']]

        if status['is_quote_status']:
            resolve_quote(status)

        resolve_status_urls(status)
        conversation_statuses = [status]

        if status['in_reply_to_status_id'] is not None:
            if status['in_reply_to_user_id'] == status['user_id']:
                return conversation_statuses
            status['replied_user'] = users[statuses[status['in_reply_to_status_id_str']]['user_id_str']]

        for status_id_str in sorted(statuses):
            next_status = statuses[status_id_str]

            if next_status['in_reply_to_status_id'] == status['id'] and next_status['user_id'] == user['id']:
                next_status['user'] = user

                if next_status['is_quote_status']:
                    resolve_quote(next_status)

                resolve_status_urls(next_status)

                conversation_statuses.append(next_status)
                status = next_status

        return conversation_statuses

    def get_user(self, id, id_type='user_id'):
        """
        Gets user for supplied user id, screen_name.

        Parameters
        ----------
        id: int or str
            The user id or screen_name
        id_type: str
            Indicates the type of the given id

        Returns
        -------
        user: dict
            The user json
        """
        if id_type not in ['user_id', 'screen_name']:
            raise RuntimeError('id_type must be user_id or screen_name')

        url = 'https://api.twitter.com/1.1/users/show.json'
        params = {id_type: id,
                  'include_blocking': True,
                  'include_blocked_by': True}
        user = self.get(url, params=params, allow_404=True).json()
        return user

    def user_lookup(self, ids, id_type='user_id'):
        """
        Gets users for supplied user ids, screen_names,
        or an iterator of user_ids of either.

        Parameters
        ----------
        ids: list
            The list of user ids or screen_names
        id_type: str
            Indicates the type of the given ids

        Returns
        -------
        users: list
            The users json
        """

        if id_type not in ['user_id', 'screen_name']:
            raise RuntimeError('id_type must be user_id or screen_name')

        ids_str = ','.join(ids)
        url = 'https://api.twitter.com/1.1/users/lookup.json'
        params = {id_type: ids_str,
                  'include_blocking': True,
                  'include_blocked_by': True}
        users = self.get(url, params=params, allow_404=True).json()
        return users

    def retweet(self, status_id):
        """
        Retweets a tweet by its id.

        Parameters
        ----------
        status_id: int
            The id of tweet

        Returns
        -------

        """
        url = f'https://api.twitter.com/1.1/statuses/retweet/{status_id}.json'
        retweeted_status = self.post(url).json()
        return retweeted_status

    @handle_rate_limit
    @handle_timeout
    def get(self, *args, **kwargs):
        if not self.client:
            self.connect()

        # Set default tweet_mode
        if 'params' not in kwargs:
            kwargs['params'] = {'tweet_mode': 'extended'}
        else:
            kwargs['params']['tweet_mode'] = 'extended'

        # Pass allow 404 to not retry on 404
        allow_404 = kwargs.pop('allow_404', False)
        try:
            r = self.client.get(*args, timeout=(3.05, 31), **kwargs)
            # this has been noticed, believe it or not
            # https://github.com/edsu/twarc/issues/75
            if r.status_code == 404 and not allow_404:
                time.sleep(1)
                r = self.get(*args, **kwargs)
            return r
        except (ssl.SSLError, ConnectionError, ProtocolError) as e:
            self.connect()
            kwargs['allow_404'] = allow_404
            return self.get(*args, **kwargs)

    @handle_rate_limit
    @handle_timeout
    def post(self, *args, **kwargs):
        if not self.client:
            self.connect()

        try:
            r = self.client.post(*args, timeout=(3.05, 31), **kwargs)
            return r
        except (ssl.SSLError, ConnectionError, ProtocolError) as e:
            self.connect()
            return self.post(*args, **kwargs)

    @handle_timeout
    def connect(self):
        """
        Sets up the HTTP session to talk to Twitter. If one is active it is
        closed and another one is opened.
        """
        if not (self.consumer_key and self.consumer_secret and self.access_token
                and self.access_token_secret):
            raise RuntimeError('MissingKeys')

        if self.client:
            self.client.close()

        self.client = OAuth1Session(client_key=self.consumer_key,
                                    client_secret=self.consumer_secret,
                                    resource_owner_key=self.access_token,
                                    resource_owner_secret=self.access_token_secret)

    def configure(self):
        """
        Authorizes a user to call api on behalf of him
        """
        request_token_url = 'https://api.twitter.com/oauth/request_token'
        base_authorization_url = 'https://api.twitter.com/oauth/authorize'
        access_token_url = 'https://api.twitter.com/oauth/access_token'

        oauth = OAuth1(self.consumer_key, client_secret=self.consumer_secret)
        r = requests.post(url=request_token_url, auth=oauth)
        credentials = parse_qs(r.text)
        resource_owner_key = credentials['oauth_token'][0]
        resource_owner_secret = credentials['oauth_token_secret'][0]

        authorize_url = f'{base_authorization_url}?oauth_token={resource_owner_key}'
        print(f'Please log into Twitter and visit this URL in your browser:\n{authorize_url}')
        verifier = input('After you have authorized the application please enter the displayed PIN: ')

        oauth = OAuth1(self.consumer_key,
                       client_secret=self.consumer_secret,
                       resource_owner_key=resource_owner_key,
                       resource_owner_secret=resource_owner_secret,
                       verifier=verifier)
        r = requests.post(url=access_token_url, auth=oauth)
        credentials = parse_qs(r.text)

        self.access_token = credentials['oauth_token'][0]
        self.access_token_secret = credentials['oauth_token_secret'][0]
