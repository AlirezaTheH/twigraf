from sqlalchemy.orm import Session

from twigraf.db.models import (Tweet,
                               Configuration,
                               TwitterUser,
                               TweetMedia)
from twigraf.db.types import TwitterUserType


def get_configuration(session):
    """
    Gets configuration from db

    Parameters
    ----------
    session: Session
        The db session

    Returns
    -------
    account: TwitterAccount
        The fetched account or None
    """
    configuration = session.query(Configuration).scalar()
    return configuration


def get_twitter_user(id, session):
    """
    Gets a twitter user from db by its id

    Parameters
    ----------
    id: int
        The id of twitter user

    session: Session
        The db session

    Returns
    -------
    account: TwitterUser
        The fetched user or None
    """
    user = session.query(TwitterUser).filter(TwitterUser.id == id).scalar()
    return user


def get_tweet(id, session):
    """
    Gets a tweet from db by its id

    Parameters
    ----------
    id: int
        The id of tweet

    session: Session
        The db session

    Returns
    -------
    tweet: Tweet
        The fetched tweet or None
    """
    tweet = session.query(Tweet).filter(Tweet.id == id).scalar()
    return tweet


def get_tweet_media(id, session):
    """
    Gets a tweet media from db by its id

    Parameters
    ----------
    id: int
        The id of tweet media

    session: Session
        The db session

    Returns
    -------
    tweet_media: TweetMedia
        The fetched tweet media or None
    """
    tweet_media = session.query(TweetMedia).filter(TweetMedia.id == id).scalar()
    return tweet_media


def get_twitter_admin_users(session):
    """
    Gets twitter admin users from db

    Parameters
    ----------
    session: Session
        The db session

    Returns
    -------
    admins: list
        List of admins
    """
    admins = session.query(TwitterUser).filter(TwitterUser.type == TwitterUserType.admin).all()
    return admins
