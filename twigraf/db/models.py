from sqlalchemy import (Column,
                        String,
                        DateTime,
                        Integer,
                        BigInteger,
                        Enum,
                        ForeignKey,
                        desc)
from sqlalchemy.orm import relationship
from sqlalchemy.orm.session import object_session

from twigraf.db.base import Base
from twigraf.db.types import (TwitterUserType,
                              TweetMediaType)


class Configuration(Base):
    __tablename__ = 'configurations'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    twitter_account_id = Column(BigInteger, nullable=False, unique=True)
    twitter_account_name = Column(String(50), nullable=False)
    twitter_account_screen_name = Column(String(15), nullable=False, unique=True)
    telegram_channel_id = Column(BigInteger, nullable=False, unique=True)
    telegram_channel_username = Column(String(32), nullable=False, unique=True)
    telegram_media_channel_id = Column(BigInteger, nullable=False, unique=True)
    telegram_media_channel_username = Column(String(32), nullable=False, unique=True)
    telegram_log_channel_id = Column(BigInteger, nullable=False, unique=True)
    telegram_log_channel_username = Column(String(32), nullable=False, unique=True)

    twitter_credentials = relationship('Credential', back_populates='configuration')

    def get_twitter_credential(self, platform):
        session = object_session(self)
        twitter_credential = session.query(TwitterCredential). \
            filter(TwitterCredential.configuration_id == self.id,
                   TwitterCredential.platform == platform).scalar()
        return twitter_credential


class TwitterCredential(Base):
    __tablename__ = 'twitter_credentials'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    configuration_id = Column(BigInteger,
                              ForeignKey('configurations.id',
                                         onupdate='CASCADE',
                                         ondelete='CASCADE',
                                         name='fk_credentials_configurations'),
                              index=True, nullable=False)
    platform = Column(String(50), nullable=False)
    access_token = Column(String(100), nullable=False)
    access_token_secret = Column(String(100), nullable=False)

    configuration = relationship('Configuration', foreign_keys=[configuration_id], back_populates='credentials')


class TwitterUser(Base):
    __tablename__ = 'twitter_users'

    id = Column(BigInteger, primary_key=True)
    name = Column(String(50), nullable=False)
    screen_name = Column(String(15), nullable=False)
    type = Column(Enum(*TwitterUserType.enums), nullable=False, default=TwitterUserType.user)

    tweets = relationship('Tweet', foreign_keys='[Tweet.twitter_user_id]', back_populates='twitter_user')
    published_tweets = relationship('TwitterUserPublishedTweet', back_populates='twitter_user')
    mentions = relationship('TweetMention', back_populates='twitter_user')


class Tweet(Base):
    __tablename__ = 'tweets'

    id = Column(BigInteger, primary_key=True)
    twitter_user_id = Column(BigInteger,
                             ForeignKey('twitter_users.id',
                                        onupdate='CASCADE',
                                        ondelete='CASCADE',
                                        name='fk_tweets_twitter_users'),
                             index=True, nullable=False)
    thread_tweet_id = Column(BigInteger,
                             ForeignKey('tweet_threads.id',
                                        onupdate='CASCADE',
                                        ondelete='CASCADE',
                                        name='fk_tweets_tweet_thread'),
                             index=True)
    text = Column(String(500), nullable=False)
    replied_tweet_id = Column(BigInteger)
    replied_twitter_user_id = Column(BigInteger,
                                     ForeignKey('twitter_users.id',
                                                onupdate='CASCADE',
                                                ondelete='CASCADE',
                                                name='fk_tweets_replied_twitter_users'),
                                     index=True)
    quoted_tweet_id = Column(BigInteger)
    quoted_twitter_user_id = Column(BigInteger,
                                    ForeignKey('twitter_users.id',
                                               onupdate='CASCADE',
                                               ondelete='CASCADE',
                                               name='fk_tweets_quoted_twitter_users'),
                                    index=True)
    creation_date = Column(DateTime, nullable=False)

    twitter_user = relationship('TwitterUser', back_populates='tweets', foreign_keys=[twitter_user_id])
    tweet_thread = relationship('TweetThread', back_populates='tweets', foreign_keys=[thread_tweet_id])
    replied_twitter_user = relationship('TwitterUser', foreign_keys=[replied_twitter_user_id])
    quoted_twitter_user = relationship('TwitterUser', foreign_keys=[quoted_twitter_user_id])
    media = relationship('TweetHasMedia', back_populates='tweet')
    hashtags = relationship('TweetHashtag', back_populates='tweet')
    mentions = relationship('TweetMention', back_populates='tweet')
    urls = relationship('TweetUrl', back_populates='tweet')
    poll_choices = relationship('TweetPollChoice', back_populates='tweet')
    published_by = relationship('TwitterUserPublishedTweet', back_populates='tweet')


class TweetThread(Base):
    __tablename__ = 'tweet_threads'

    id = Column(BigInteger, primary_key=True)
    tweets = relationship('Tweet', back_populates='tweet_thread')


class TweetMedia(Base):
    __tablename__ = 'tweet_media'

    id = Column(BigInteger, primary_key=True)
    type = Column(Enum(*TweetMediaType.enums), nullable=False)
    url = Column(String(300), nullable=False)

    tweets = relationship('TweetHasMedia', back_populates='media')
    video_variants = relationship('TweetMediaVideoVariant', back_populates='tweet_media')

    def biggest_variant(self, smaller_than):
        session = object_session(self)
        video_variant = session.query(TweetMediaVideoVariant). \
            order_by(desc(TweetMediaVideoVariant.size)). \
            filter(TweetMediaVideoVariant.tweet_media_id == self.id,
                   TweetMediaVideoVariant.size < smaller_than).first()
        return video_variant

    def biggest_sent_variant(self, bigger_than):
        session = object_session(self)
        video_variant = session.query(TweetMediaVideoVariant). \
            order_by(desc(TweetMediaVideoVariant.size)). \
            filter(TweetMediaVideoVariant.tweet_media_id == self.id,
                   TweetMediaVideoVariant.size >= bigger_than,
                   (TweetMediaVideoVariant.telegram_bot_file_id is not None)
                   | (TweetMediaVideoVariant.telegram_media_message_id is not None)).first()
        return video_variant


class TweetMediaVideoVariant(Base):
    __tablename__ = 'tweet_media_video_variants'

    id = Column(BigInteger, primary_key=True)
    tweet_media_id = Column(BigInteger,
                            ForeignKey('tweet_media.id',
                                       onupdate='CASCADE',
                                       ondelete='CASCADE',
                                       name='fk_tweet_media_video_variants_tweet_media'),
                            index=True, nullable=False)
    url = Column(String(300), nullable=False)
    telegram_bot_file_id = Column(String(200))
    telegram_media_message_id = Column(Integer)
    size = Column(Integer, nullable=False)
    duration_seconds = Column(Integer, nullable=False)
    width = Column(Integer, nullable=False)
    height = Column(Integer, nullable=False)

    tweet_media = relationship('TweetMedia', foreign_keys=[tweet_media_id], back_populates='video_variants')


class TweetHasMedia(Base):
    __tablename__ = 'tweet_has_media'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    tweet_id = Column(BigInteger,
                      ForeignKey('tweets.id',
                                 onupdate='CASCADE',
                                 ondelete='CASCADE',
                                 name='fk_tweet_has_media_tweets'),
                      index=True, nullable=False)
    media_id = Column(BigInteger,
                      ForeignKey('tweet_media.id',
                                 onupdate='CASCADE',
                                 ondelete='CASCADE',
                                 name='fk_tweet_has_media_tweet_media'),
                      index=True, nullable=False)

    tweet = relationship('Tweet', foreign_keys=[tweet_id], back_populates='media')
    media = relationship('TweetMedia', foreign_keys=[media_id], back_populates='tweets')


class TweetHashtag(Base):
    __tablename__ = 'tweet_hashtags'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    tweet_id = Column(BigInteger,
                      ForeignKey('tweets.id',
                                 onupdate='CASCADE',
                                 ondelete='CASCADE',
                                 name='fk_tweet_hashtags_tweets'),
                      index=True, nullable=False)
    text = Column(String(500), nullable=False)
    start_index = Column(Integer, nullable=False)
    end_index = Column(Integer, nullable=False)

    tweet = relationship('Tweet', foreign_keys=[tweet_id], back_populates='hashtags')


class TweetMention(Base):
    __tablename__ = 'tweet_mentions'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    tweet_id = Column(BigInteger,
                      ForeignKey('tweets.id',
                                 onupdate='CASCADE',
                                 ondelete='CASCADE',
                                 name='fk_tweet_mentions_tweets'),
                      index=True, nullable=False)
    twitter_user_id = Column(BigInteger,
                             ForeignKey('twitter_users.id',
                                        onupdate='CASCADE',
                                        ondelete='CASCADE',
                                        name='fk_tweet_mentions_twitter_users'),
                             index=True, nullable=False)
    start_index = Column(Integer, nullable=False)
    end_index = Column(Integer, nullable=False)

    tweet = relationship('Tweet', foreign_keys=[tweet_id], back_populates='mentions')
    twitter_user = relationship('TwitterUser', foreign_keys=[twitter_user_id], back_populates='mentions')


class TweetUrl(Base):
    __tablename__ = 'tweet_urls'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    tweet_id = Column(BigInteger,
                      ForeignKey('tweets.id',
                                 onupdate='CASCADE',
                                 ondelete='CASCADE',
                                 name='fk_tweet_urls_tweets'),
                      index=True, nullable=False)
    url = Column(String(300), nullable=False)
    short_url = Column(String(300), nullable=False)
    title = Column(String(200))
    start_index = Column(Integer, nullable=False)
    end_index = Column(Integer, nullable=False)

    tweet = relationship('Tweet', foreign_keys=[tweet_id], back_populates='urls')


class TweetPollChoice(Base):
    __tablename__ = 'tweet_poll_choices'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    tweet_id = Column(BigInteger,
                      ForeignKey('tweets.id',
                                 onupdate='CASCADE',
                                 ondelete='CASCADE',
                                 name='fk_tweet_poll_choices_tweets'),
                      index=True, nullable=False)
    label = Column(String(25), nullable=False)

    tweet = relationship('Tweet', foreign_keys=[tweet_id], back_populates='poll_choices')


class TwitterUserPublishedTweet(Base):
    __tablename__ = 'twitter_user_published_tweet'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    twitter_user_id = Column(BigInteger,
                             ForeignKey('twitter_users.id',
                                        onupdate='CASCADE',
                                        ondelete='CASCADE',
                                        name='fk_twitter_user_published_tweet_twitter_users'),
                             index=True, nullable=False)
    tweet_id = Column(BigInteger,
                      ForeignKey('tweets.id',
                                 onupdate='CASCADE', ondelete='CASCADE',
                                 name='fk_twitter_user_published_tweet_tweets'),
                      index=True, nullable=False)
    url_tweet_id = Column(BigInteger, nullable=False)
    creation_date = Column(DateTime, nullable=False)

    twitter_user = relationship('TwitterUser',
                                foreign_keys=[twitter_user_id],
                                back_populates='published_tweets')
    tweet = relationship('Tweet', foreign_keys=[tweet_id], back_populates='published_by')
