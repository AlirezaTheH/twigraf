from collections import namedtuple

from twigraf.utils.types import Enum

# Represents a twitter card meta information.
card = namedtuple('Card', ['has_media',
                           'media_type',
                           'photo_url_tag',
                           'video_url_tag',
                           'video_has_duration',
                           'has_extra_url',
                           'has_url',
                           'url_title_tag',
                           'url_position',
                           'url_tag',
                           'short_url_tag',
                           'url_may_redirect',
                           'url_auto_created'])


class TweetMediaType(Enum):
    """
    Represents a tweet media type.
    """
    photo = 'photo'
    video = 'video'
    animated_gif = 'animated_gif'


class TwitterUserType(Enum):
    """
    Represents a twitter user's type.
    """
    user = 'user'
    admin = 'admin'
