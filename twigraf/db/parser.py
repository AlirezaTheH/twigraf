import re
from urllib.parse import unquote

import requests
import bs4
from sqlalchemy.orm import Session

from twigraf.db.models import (Tweet,
                               TweetMedia,
                               TweetMediaVideoVariant,
                               TweetHasMedia,
                               TweetHashtag,
                               TweetMention,
                               TwitterUser,
                               TweetPollChoice,
                               TweetUrl)
from twigraf.db.queries import (get_twitter_user,
                                get_tweet_media)
from twigraf.db.types import (card,
                              TweetMediaType)
from twigraf.utils.web import (get_file_size,
                               get_final_url_title)
from twigraf.utils.video import get_video_duration
from twigraf.twitter.utils import parse_datetime


def new_tweet_from_json(status, session, tweet_thread=None):
    """
    Creates new tweet instance based on a json status.

    Parameters
    ----------
    status: dict
        The json status

    session: Session
        The db session

    tweet_thread: TweetThread
        The tweet thread object if tweet is part of a thread

    Returns
    -------
    tweet: Tweet
        Created tweet instance
    """
    tweet = Tweet(id=status['id'],
                  text=status['full_text'],
                  creation_date=parse_datetime(status['created_at']))

    twitter_user = new_twitter_user_from_json(status['user'], session)
    tweet.twitter_user = twitter_user

    entities = status['entities']

    hashtags = [new_tweet_hashtag_from_json(h) for h in entities['hashtags']]
    if len(hashtags) > 0:
        tweet.hashtags = hashtags

    mentions = [new_tweet_mention_from_json(um, session) for um in entities['user_mentions']]
    if len(mentions) > 0:
        tweet.mentions = mentions

    if 'extended_entities' in status:
        entities['media'] = status['extended_entities']['media']

    media = []
    if 'media' in entities:
        media = [TweetHasMedia(media=new_tweet_media_from_json(m, session)) for m in entities['media']]

    if 'replied_user' in status:
        tweet.replied_tweet_id = status['in_reply_to_status_id']
        tweet.replied_twitter_user = new_twitter_user_from_json(status['replied_user'], session)

    if status['is_quote_status']:
        tweet.quoted_tweet_id = status['quoted_status_id']
        tweet.quoted_twitter_user = new_twitter_user_from_json(status['quoted_user'], session)

        # If quote is not a simple quote remove the extra link
        permalink_url = status['quoted_status_permalink']['url']
        permalink_pattern = re.compile(re.escape(permalink_url))
        if re.search(permalink_pattern, tweet.text):
            tweet.text = re.sub(permalink_pattern, '', tweet.text)
            entities['urls'] = entities['urls'][:-1]

    poll_choices = []
    card_media = None
    card_url = None
    if 'card' in status:
        poll_choices, card_media, card_url = extract_card_data_from_json(tweet, status, session)

    if len(poll_choices) > 0:
        tweet.poll_choices = poll_choices

    if card_media is not None:
        media.append(TweetHasMedia(media=card_media))
    if len(media) > 0:
        tweet.media = media

    if card_url is None:
        urls = [new_tweet_url_from_json(u) for u in entities['urls']]
    else:
        urls = [new_tweet_url_from_json(u) for u in entities['urls'][:-1]] + [card_url]
    if len(urls) > 0:
        tweet.urls = urls

    if tweet_thread is not None:
        tweet.tweet_thread = tweet_thread

    session.add(tweet)
    session.commit()

    return tweet


def new_twitter_user_from_json(user_json, session):
    """
    Creates new twitter user instance based on a json user.

    Parameters
    ----------
    user_json: dict
        The json user

    session: Session
        The db session

    Returns
    -------
    twitter_user: TwitterUser
        Created twitter user instance
    """
    id = user_json['id']
    name = user_json['name']
    screen_name = user_json['screen_name']

    twitter_user = get_twitter_user(id, session)
    if twitter_user is not None:
        twitter_user.name = name
        twitter_user.screen_name = screen_name
        return twitter_user

    twitter_user = TwitterUser(id=id,
                               name=name,
                               screen_name=screen_name)

    return twitter_user


def new_tweet_hashtag_from_json(hashtag_json):
    """
    Creates a new tweet hashtag instance based on a json hashtag.

    Parameters
    ----------
    hashtag_json: dict
        The json hashtag

    Returns
    -------
        tweet_hashtag: TweetHashtag
         Created tweet hashtag instance
    """
    text = hashtag_json['text']
    start_index, end_index = hashtag_json['indices']
    tweet_hashtag = TweetHashtag(text=text,
                                 start_index=start_index,
                                 end_index=end_index)
    return tweet_hashtag


def new_tweet_mention_from_json(mention_json, session):
    """
    Creates a new tweet mention instance based on a json mention.

    Parameters
    ----------
    mention_json: dict
        The json mention

    session: Session
        The db session

    Returns
    -------
    tweet_mention: TweetMention
        Created tweet mention instance

    """
    # Hint: mention json can act like a user json
    twitter_user = new_twitter_user_from_json(mention_json, session)
    start_index, end_index = mention_json['indices']
    tweet_mention = TweetMention(start_index=start_index,
                                 end_index=end_index)
    tweet_mention.twitter_user = twitter_user

    return tweet_mention


def new_tweet_url_from_json(url_json):
    """
    Creates a new tweet url instance based on a json url.

    Parameters
    ----------
    url_json: dict
        The json url

    Returns
    -------
    tweet_url: TweetUrl
        Created tweet url instance
    """
    url = url_json['expanded_url']
    short_url = url_json['url']
    start_index, end_index = url_json['indices']

    if url_json['is_status_url']:
        title = url_json['status_user']['name']
    else:
        url, title = get_final_url_title(url)

    tweet_url = TweetUrl(url=url,
                         short_url=short_url,
                         title=title,
                         start_index=start_index,
                         end_index=end_index)
    return tweet_url


def new_tweet_media_from_json(media_json, session):
    """
    Creates a new tweet media instance based on a json media.

    Parameters
    ----------
    media_json: dict
        The json media

    session: Session
        The db session

    Returns
    -------
    tweet_media: TweetMedia
        Created tweet media instance

    """
    media_id = media_json['id']

    tweet_media = get_tweet_media(media_id, session)
    if tweet_media is not None:
        return tweet_media

    media_type = media_json['type']
    tweet_media = TweetMedia(id=media_id, type=media_type, url=media_json['media_url_https'])

    if media_type == TweetMediaType.animated_gif:
        tweet_media.url = media_json['video_info']['variants'][0]['url']

    elif media_type == TweetMediaType.video:
        variant_pattern = re.compile(r'(?P<url>.+/(?P<width>\d+)x(?P<height>\d+)/.+\.mp4).*$')
        video_variants = []
        video_info = media_json['video_info']
        for variant in video_info['variants']:
            if variant['content_type'] == 'video/mp4':
                variant_match = re.match(variant_pattern, variant['url'])
                url = variant_match.group('url')
                video_variants.append(TweetMediaVideoVariant(url=url,
                                                             size=get_file_size(url),
                                                             width=int(variant_match.group('width')),
                                                             height=int(variant_match.group('height')),
                                                             duration_seconds=round(
                                                                 video_info['duration_millis'] / 1000)))
        tweet_media.video_variants = video_variants

    return tweet_media


def extract_card_data_from_json(tweet, status, session):
    """
    Extracts poll, media and url data from card.

    Parameters
    ----------
    tweet: Tweet
        The tweet object

    status: dict
        The json status

    session: Session
        The db session

    Returns
    -------
    result: tuple
        Tuple of poll choices, card media and card url
    """
    cards = {
        'poll_text_only':
            card(False, None, None, None, None, False, False, None, None, None, None, None, None),

        'poll_image':
            card(True, 'photo', 'image_original', None, None, False, False, None, None, None, None, None, None),

        'poll_video':
            card(True, 'video', 'player_image_original', 'player_stream_url', True, False, False, None, None, None,
                 None, None, None),

        'summary':
            card(False, None, None, None, None, False, True, 'title', 'urls', None, None, True, False),

        'summary_large_image':
            card(True, 'photo', 'summary_photo_image_original', None, None, False, True, 'title', 'urls', None, None,
                 True, False),

        'promo_website':
            card(True, 'photo', 'promo_image_original', None, None, False, True, 'title', 'binding_values',
                 'website_dest_url', 'website_shortened_url', False, False),

        'promo_video_website':
            card(True, 'video', 'player_image_original', 'player_stream_url', True, False, True, 'title',
                 'binding_values', 'website_dest_url', 'website_shortened_url', False, False),

        'broadcast':
            card(True, 'photo', 'broadcast_thumbnail_original', None, None, False, True, 'broadcast_title', 'urls',
                 None, None, False, True),

        'periscope_broadcast':
            card(True, 'photo', 'summary_photo_image_original', None, None, False, True, 'status', 'urls', None, None,
                 False, True),

        'live_event':
            card(True, 'photo', 'event_thumbnail_original', None, None, False, True, 'event_title', 'urls', None, None,
                 False, True),

        'amplify':
            card(True, 'video', 'player_image', 'amplify_url_vmap', False, True, False, None, None, None, None, None,
                 None),

        'player':
            card(False, None, None, None, None, False, True, 'title', 'urls', 'player_url', None, False,
                 False)
    }

    poll_choices = []
    card_media = None
    card_url = None
    card_json = status['card']

    # If card name starts with a number remove it
    number_started_card_name_pattern = re.compile(r'^\d+:')
    card_name = re.sub(number_started_card_name_pattern, '', card_json['name'])

    binding_values = card_json['binding_values']

    # If card is a poll card extract poll choices
    poll_pattern = re.compile(r'poll(?P<number_of_choices>[2-4])choice_(?P<type>.+)')
    poll_match = re.match(poll_pattern, card_name)
    if poll_match:
        number_of_choices = int(poll_match.group('number_of_choices'))
        poll_choices = [TweetPollChoice(label=binding_values[f'choice{cn}_label']['string_value'])
                        for cn in range(1, number_of_choices + 1)]
        card_name = f'poll_{poll_match.group("type")}'

    card_meta = cards[card_name]

    if card_meta.has_media:
        card_photo_url = binding_values[card_meta.photo_url_tag]['image_value']['url']
        media_id_pattern = re.compile(r'/(?P<id>\d+)([/\.]|$)')

        if card_meta.media_type == TweetMediaType.photo:
            if re.search(media_id_pattern, card_photo_url):
                media_id = int(re.search(media_id_pattern, card_photo_url).group('id'))
            elif re.search(media_id_pattern, card_json['url']):
                media_id = int(re.search(media_id_pattern, card_json['url']).group('id'))
            else:
                media_id = status['id']

            card_media = get_tweet_media(media_id, session)
            if card_media is None:
                card_media = TweetMedia(id=media_id,
                                        type='photo',
                                        url=card_photo_url)

        elif card_meta.media_type == TweetMediaType.video:
            vmap_url = binding_values[card_meta.video_url_tag]['string_value']
            media_id = int(re.search(media_id_pattern, vmap_url).group('id'))

            card_media = get_tweet_media(media_id, session)
            if card_media is None:
                xml = requests.get(vmap_url).text

                if card_meta.video_has_duration:
                    duration_seconds = int(binding_values['content_duration_seconds']['string_value'])
                else:
                    duration_soup = bs4.BeautifulSoup(xml, 'html.parser')
                    url = duration_soup.find(text=lambda tag: isinstance(tag, bs4.CData)).string.strip()
                    duration_seconds = get_video_duration(url)

                variant_pattern = re.compile(r'(?P<url>.+/(?P<width>\d+)x(?P<height>\d+)/.+\.mp4).*$')
                variants_soup = bs4.BeautifulSoup(xml, 'lxml')
                video_variants = []

                for variant in variants_soup('tw:videovariant'):
                    if variant['content_type'] == 'video/mp4':
                        variant_match = re.match(variant_pattern, unquote(variant['url']))
                        url = variant_match.group('url')
                        video_variants.append(TweetMediaVideoVariant(
                            url=url,
                            size=get_file_size(url),
                            width=int(variant_match.group('width')),
                            height=int(variant_match.group('height')),
                            duration_seconds=duration_seconds))

                card_media = TweetMedia(id=media_id,
                                        type='video',
                                        url=card_photo_url,
                                        video_variants=video_variants)

    if card_meta.has_extra_url:
        status['entities']['urls'] = status['entities']['urls'][:-1]

    if card_meta.has_url:
        url_title = binding_values[card_meta.url_title_tag]['string_value']
        urls_json = status['entities']['urls']

        if card_meta.url_position == 'urls':
            url_json = urls_json[-1]
            if card_meta.url_tag is not None:
                url = binding_values[card_meta.url_tag]['string_value']
            else:
                url = url_json['expanded_url']
            if card_meta.short_url_tag is not None:
                short_url = binding_values[card_meta.short_url_tag]['string_value']
            else:
                short_url = url_json['url']
            start_index, end_index = url_json['indices']

            if card_meta.url_auto_created:
                tweet.text = f'{tweet.text[:start_index]}\n{tweet.text[start_index:]}'
                start_index += 1
                end_index += 1
                url_json['indices'] = [start_index, end_index]

        elif card_meta.url_position == 'binding_values':
            url = binding_values[card_meta.url_tag]['string_value']
            short_url = binding_values.get(card_meta.short_url_tag, {}).get('string_value', url)
            tweet_text_length = len(tweet.text)
            tweet.text += f'\n{short_url}'
            start_index, end_index = tweet_text_length + 1, len(tweet.text)
            urls_json.append({'url': short_url, 'expanded_url': url, 'indices': [start_index, end_index]})

        if card_meta.url_may_redirect:
            if binding_values['vanity_url']['string_value'] not in url:
                url, url_title = get_final_url_title(url)

        card_url = TweetUrl(url=url,
                            short_url=short_url,
                            title=url_title,
                            start_index=start_index,
                            end_index=end_index)

    return poll_choices, card_media, card_url
