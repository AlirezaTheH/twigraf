from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class SessionFactory:
    def __init__(self,
                 database_engine,
                 database_user,
                 database_password,
                 database_host,
                 database_port,
                 database_name,
                 recreate_tables):
        """
        Initializes session factory

        Parameters
        ----------
        database_engine: str
            The db engine

        database_user: str
            The db user

        database_password: str
            The db password

        database_host: str
            The db host

        database_port: str
            The db port

        database_name: str
            The db name

        recreate_tables: bool
            If true recreate tables.
        """
        self.strategy = '{db_engine}://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}'
        self.engine = create_engine(self.strategy.format(db_engine=database_engine,
                                                         db_user=database_user,
                                                         db_password=database_password,
                                                         db_host=database_host,
                                                         db_port=database_port,
                                                         db_name=database_name),
                                    pool_recycle=3600)
        if recreate_tables:
            Base.metadata.drop_all(self.engine)
            Base.metadata.create_all(self.engine)

    def new_session(self):
        return sessionmaker(bind=self.engine, autoflush=False)()
