import logging

from telethon.errors import WebpageCurlFailedError

logger = logging.getLogger('twigraf')


def handle_server_cache_bug(function):
    """
    A decorator to handle telegram server cache problem that sometimes may happen.
    If this happens will send a message to Webpage Bot to update cache manually.
    """
    async def new_function(*args, **kwargs):
        try:
            return await function(*args, **kwargs)
        except WebpageCurlFailedError as e:
            logger.warning('Telegram server cache problem happened!')
            self = args[0]
            media = args[2]
            await self.user_client.send_message('WebpageBot', media)
            return await new_function(*args, **kwargs)

    return new_function
