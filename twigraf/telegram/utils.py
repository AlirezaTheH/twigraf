import re

from twigraf.db.models import (Configuration,
                               Tweet)
from twigraf.utils.string import isub


def resolve_links(tweet):
    """
    Converts user mentions to links to the twitter profile,
    hashtags to links to the twitter hashtag
    and urls to hyperlink with their title

    Parameters
    ----------
    tweet: Tweet
        The tweet object

    Returns
    -------
    result: str
        The processed tweet text
    """

    def replacement(start_index, end_index):
        if [start_index, end_index] in mention_indices:
            mention = tweet.mentions[mention_indices.index([start_index, end_index])]
            link = f'https://twitter.com/{mention.twitter_user.screen_name}'
            text = tweet.text[start_index: end_index]

        elif [start_index, end_index] in hashtag_indices:
            hashtag = tweet.hashtags[hashtag_indices.index([start_index, end_index])]
            link = f'https://twitter.com/hashtag/{hashtag.text}'
            text = tweet.text[start_index: end_index]

        elif [start_index, end_index] in url_indices:
            url = tweet.urls[url_indices.index([start_index, end_index])]
            link = url.url
            text = url.title

        html = f'<a href="{link}">{text}</a>'
        return html

    text = tweet.text
    mention_indices = [[um.start_index, um.end_index] for um in tweet.mentions]

    # If tweet is a reply ignore the first mention
    if tweet.replied_tweet_id is not None:
        if len(mention_indices) > 0:
            indices = mention_indices[0]
            text = isub([indices], (indices[1] - indices[0]) * ' ', text)
            mention_indices = mention_indices[1:]

    hashtag_indices = [[h.start_index, h.end_index] for h in tweet.hashtags]
    url_indices = [[u.start_index, u.end_index] for u in tweet.urls]
    all_indices = mention_indices + hashtag_indices + url_indices
    all_indices.sort()

    return isub(all_indices, replacement, text)


def remove_extra_twitter_link(text):
    """
    Removes extra twitter link that appears at end of the tweet.

    Parameters
    ----------
    text: str
        The tweet text

    Returns
    -------
    result: str
        The processed tweet text
    """
    return re.sub(r'https://t\.co/[a-zA-Z\d]{10}$', '', text)


def prepare_tweet_text(tweet, configuration, url_tweet_id=None):
    """
    Do all things for tweet text.

    Parameters
    ----------
    tweet: Tweet
        The tweet object

    configuration:
        The configuration to use the bot

    url_tweet_id: int
        If the tweet should be sent with a url, tweet id for url

    Returns
    -------
    result: str
        The prepared tweet text
    """
    status_url = 'https://twitter.com/{screen_name}/status/{status_id}'
    text = resolve_links(tweet)
    text = remove_extra_twitter_link(text)
    text = text.strip()

    if tweet.replied_tweet_id is not None:
        link = status_url.format(screen_name=tweet.replied_twitter_user.screen_name,
                                 status_id=tweet.replied_tweet_id)
        html = f'در پاسخ به <a href="{link}">{tweet.replied_twitter_user.name}</a>\n'
        text = html + text

    if tweet.quoted_tweet_id is not None:
        link = status_url.format(screen_name=tweet.quoted_twitter_user.screen_name,
                                 status_id=tweet.quoted_tweet_id)
        html = f'\nبا نقل قول از <a href="{link}">{tweet.quoted_twitter_user.name}</a>'
        text += html

    if url_tweet_id is not None:
        link = status_url.format(screen_name=configuration.twitter_account_screen_name,
                                 status_id=url_tweet_id)
        html = f'\n\n<a href="{link}">{tweet.twitter_user.name}</a>'
        text += html

    text = text.strip()
    return text
