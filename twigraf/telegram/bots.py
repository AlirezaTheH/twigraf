import os
import re
import asyncio

from telethon import TelegramClient
from telethon.utils import pack_bot_file_id
from telethon.tl.types import (Message,
                               DocumentAttributeVideo,
                               InputMediaPoll,
                               Poll,
                               PollAnswer,
                               InputMediaPhotoExternal)

from twigraf.db.models import Configuration
from twigraf.db.types import TweetMediaType
from twigraf.utils.web import download_file
from twigraf.utils.video import create_video_thumbnail
from twigraf.utils.fast_telethon import upload_file
from twigraf.telegram.utils import prepare_tweet_text
from twigraf.telegram.types import (presend_media,
                                    PresendType,
                                    NetworkUsageLevel)
from twigraf.telegram.decorators import handle_server_cache_bug


class TweetBot:
    """
    Represents a Telegram Bot able to send tweets to a channel.

    Attributes
    ----------
    max_bot_filesize_download: int
        Telegram max bot filesize download

    max_bot_filesize_upload: int
        Telegram max bot filesize upload

    max_user_filesize_upload: int
        Telegram max user filesize upload

    max_filesize: int
        Bot max file filesize

    user_client: TelegramClient
        The user client

    bot_client: TelegramClient
        The bot client

    loop: AsyncLoop
        The async loop

    configuration: TwitterAccount
        The configuration to use the bot
    """

    def __init__(self, api_id, api_hash, phone, password, token, configuration, network_usage_level):
        """
        Initiates the bot.

        Parameters
        ----------
        api_id: int
            Telegram api id

        api_hash: str
            Telegram api hash

        phone: str
            Telegram phone

        password: str
            Telegram password

        token: str
            Bot's unique authentication

        configuration: Configuration
            The configuration to use the bot

        network_usage_level: str
            Indicates network usage level. see NetworkUsageLevel for available levels.
        """
        # 20MB
        self.max_bot_filesize_download = 20*2**20

        # 50MB
        self.max_bot_filesize_upload = 50*2**20

        # 2GB
        self.max_user_filesize_upload = 2*2**30

        network_usage_to_max_filesize = {
            NetworkUsageLevel.low: self.max_bot_filesize_download,
            NetworkUsageLevel.medium: self.max_bot_filesize_upload,
            NetworkUsageLevel.high: self.max_user_filesize_upload
        }
        self.max_filesize = network_usage_to_max_filesize[network_usage_level]
        self.user_client = TelegramClient('user', api_id, api_hash).start(phone=lambda: phone,
                                                                          password=lambda: password)
        self.bot_client = TelegramClient('bot', api_id, api_hash).start(bot_token=token)
        self.user_client.parse_mode = 'html'
        self.bot_client.parse_mode = 'html'
        self.loop = asyncio.get_event_loop()
        self.configuration = configuration

    def send_tweet(self, tweet, url_tweet_id=None, reply_to=None):
        """
        Sends a tweet to connected channel of twitter account in a appropriate way.

        Parameters
        ----------
        tweet: Tweet
            The tweet object to be sent

        url_tweet_id: int
            If the tweet should be sent with a url, tweet id for url

        reply_to: Message
            If the tweet is a reply, id of the original message

        Returns
        -------
        message: Message
            On success, the sent message is returned.
        """
        chat_id = self.configuration.telegram_channel_id
        text = prepare_tweet_text(tweet, self.configuration, url_tweet_id)
        presends = self.get_presend_media(tweet)
        if len(presends) == 0:
            message = self.loop.run_until_complete(self.bot_client.send_message(chat_id,
                                                                                text,
                                                                                link_preview=False,
                                                                                reply_to=reply_to))

        elif len(presends) == 1:
            presend = presends[0]
            if presend.media_type is TweetMediaType.photo:
                message = self.loop.run_until_complete(self.send_media(chat_id,
                                                                       InputMediaPhotoExternal(presend.media),
                                                                       caption=text,
                                                                       reply_to=reply_to))
            elif presend.media_type is TweetMediaType.animated_gif:
                message = self.loop.run_until_complete(self.send_media(chat_id,
                                                                       presend.media,
                                                                       caption=text,
                                                                       reply_to=reply_to))
            else:
                message = self.loop.run_until_complete(self.send_video(chat_id,
                                                                       presend,
                                                                       caption=text,
                                                                       reply_to=reply_to))

        elif len(presends) > 1:
            media_chat_id = self.configuration.telegram_media_channel_id
            message = self.loop.run_until_complete(self.send_photo_group(chat_id,
                                                                         media_chat_id,
                                                                         presends,
                                                                         caption=text,
                                                                         reply_to=reply_to))

        for p in presends:
            if p.type is PresendType.file or p.type is PresendType.large_file:
                os.remove(p.media)
                os.remove(p.thumbnail)

        if len(tweet.poll_choices) > 0:
            poll = InputMediaPoll(Poll(id=0,
                                       question='گزینه‌ها:',
                                       answers=[PollAnswer(pc.label, bytes([i]))
                                                for i, pc in enumerate(tweet.poll_choices)]))
            return self.loop.run_until_complete(self.bot_client.send_message(chat_id,
                                                                             file=poll,
                                                                             reply_to=message))

        return message

    def send_thread(self, tweets, url_tweet_id):
        """
        Sends a tweet thread to connected channel of twitter account in a appropriate way.

        Parameters
        ----------
        tweets: list
            List of tweet objects in the thread

        url_tweet_id: int
            The tweet id for url of thread

        Returns
        -------
        message: telegram.Message
            On success, the last tweet sent message is returned.
        """
        message = self.send_tweet(tweets[0])
        for tweet in tweets[1:-1]:
            message = self.send_tweet(tweet, reply_to=message)
        return self.send_tweet(tweets[-1], url_tweet_id, message)

    def get_presend_media(self, tweet):
        """
        Prepares tweet media for sending.

        Parameters
        ----------
        tweet: Tweet
            The tweet object

        Returns
        -------
        presends: list
            List of prepared media
        """
        presends = []
        for m in tweet.media:
            thumbnail = None
            media_type = m.media.type
            if m.media.type is not TweetMediaType.video:
                media = m.media.url
                presend_type = PresendType.url
                variant = None
            else:
                variant = m.media.biggest_sent_variant(self.max_filesize)
                if variant is not None:
                    if variant.telegram_bot_file_id is not None:
                        media = variant.telegram_bot_file_id
                        presend_type = PresendType.bot_file_id
                    else:
                        media = variant.telegram_media_message_id
                        presend_type = PresendType.media_message_id
                else:
                    variant = m.media.biggest_variant(self.max_filesize)
                    if variant is None:
                        media = m.media.url
                        media_type = TweetMediaType.photo
                        presend_type = PresendType.url
                    else:
                        if variant.telegram_bot_file_id is not None:
                            media = variant.telegram_bot_file_id
                            presend_type = PresendType.bot_file_id
                        elif variant.telegram_media_message_id is not None:
                            media = variant.telegram_media_message_id
                            presend_type = PresendType.media_message_id
                        else:
                            if variant.size >= self.max_bot_filesize_download:
                                media = download_file(variant.url)
                                jpg_pattern = re.compile(r'.+\.jpg$')
                                if re.match(jpg_pattern, m.media.url) is None:
                                    thumbnail = create_video_thumbnail(media)
                                else:
                                    thumbnail = download_file(m.media.url)

                                if variant.size >= self.max_bot_filesize_upload:
                                    presend_type = PresendType.large_file
                                else:
                                    presend_type = PresendType.file
                            else:
                                media = variant.url
                                presend_type = PresendType.url
            presends.append(presend_media(media, media_type, presend_type, variant, thumbnail))

        return presends

    @handle_server_cache_bug
    async def send_media(self, *args, **kwargs):
        return await self.bot_client.send_file(*args, **kwargs)

    async def send_video(self,
                         chat_id,
                         presend,
                         caption,
                         reply_to):
        if presend.type is PresendType.media_message_id:
            media = await self.user_client.get_messages(chat_id, ids=presend.media)
            message = await self.user_client.send_file(chat_id, media, caption=caption, reply_to=reply_to)

        elif presend.type is PresendType.large_file:
            with open(presend.media, 'rb') as file:
                input_file = await upload_file(self.user_client, file)
            message = await self.user_client.send_file(chat_id,
                                                       input_file,
                                                       caption=caption,
                                                       attributes=[
                                                           DocumentAttributeVideo(presend.variant.duration_seconds,
                                                                                  presend.variant.width,
                                                                                  presend.variant.height,
                                                                                  supports_streaming=True)],
                                                       thumb=presend.thumbnail,
                                                       supports_streaming=True,
                                                       reply_to=reply_to)
            presend.variant.telegram_media_message_id = message.id
        elif presend.type is PresendType.file:
            with open(presend.media, 'rb') as file:
                input_file = await upload_file(self.bot_client, file)
            message = await self.bot_client.send_file(chat_id,
                                                      input_file,
                                                      caption=caption,
                                                      attributes=[
                                                          DocumentAttributeVideo(presend.variant.duration_seconds,
                                                                                 presend.variant.width,
                                                                                 presend.variant.height,
                                                                                 supports_streaming=True)],
                                                      thumb=presend.thumbnail,
                                                      supports_streaming=True,
                                                      reply_to=reply_to)
            presend.variant.telegram_bot_file_id = pack_bot_file_id(message.media)
        else:
            message = await self.send_media(chat_id,
                                            presend.media,
                                            caption=caption,
                                            reply_to=reply_to)
        return message

    async def send_photo_group(self, chat_id, media_chat_id, presends, caption, reply_to):
        media = [await self.send_media(media_chat_id, InputMediaPhotoExternal(presend.media)) for presend in presends]
        messages = await self.bot_client.send_file(chat_id,
                                                   media,
                                                   caption=caption,
                                                   reply_to=reply_to)
        return messages[0]
