from collections import namedtuple

from twigraf.utils.types import Enum

# Represents a tweet media meta information before send.
presend_media = namedtuple('PresendMedia', ['media',
                                            'media_type',
                                            'type',
                                            'variant',
                                            'thumbnail'])


class PresendType(Enum):
    """
    Represents a presend type.
    """
    url = 'url'
    bot_file_id = 'bot_file_id'
    media_message_id = 'media_message_id'
    file = 'file'
    large_file = 'large_file'


class NetworkUsageLevel(Enum):
    """
    Represents a tweet media type.
    """
    low = 'low'
    medium = 'medium'
    high = 'high'
