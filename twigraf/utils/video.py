import subprocess


def get_video_duration(input_file):
    """
    Gets the duration of a video using ffprobe.

    Parameters
    ----------
    input_file: str
        The video url or path

    Returns
    -------
    result: int
        The video duration seconds
    """
    output = subprocess.check_output(f'ffprobe -i {input_file} -show_entries format=duration -v quiet -of csv="p=0"',
                                     text=True,
                                     shell=True,
                                     stderr=subprocess.STDOUT)
    return round(float(output.strip()))


def create_video_thumbnail(input_file):
    """
    Creates thumbnail for a given video using ffmpeg.

    Parameters
    ----------
    input_file: str
        The video url or path

    Returns
    -------
    thumbnail_filename: str
        Created thumbnail file name
    """
    video_filename = input_file.split('/')[-1]
    thumbnail_filename = f'{video_filename}.jpg'
    subprocess.check_call(f'ffmpeg -i {video_filename} -ss 00:00:00.000 -vframes 1 {thumbnail_filename}',
                          text=True,
                          shell=True,
                          stderr=subprocess.STDOUT,
                          stdout=subprocess.PIPE)
    return thumbnail_filename
