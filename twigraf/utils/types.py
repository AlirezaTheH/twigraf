import re

from twigraf.utils.decorators import classproperty


class Enum:
    @classproperty
    def enums(cls):
        return [var for var in vars(cls) if re.search(r'^(_|enums)', var) is None]