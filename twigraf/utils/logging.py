import asyncio
import logging
from logging import (Handler,
                     Formatter,
                     LogRecord)
from io import BytesIO

from telethon import TelegramClient
from emoji import emojize


class TelegramHandler(Handler):
    """
    Telegram logging handler

    Attributes
    ----------
    bot_client: TelegramClient
        The telegram client

    loop: AsyncLoop
        The async loop

    chat_id: int
        The Telegram log chat id
    """
    def __init__(self, api_id, api_hash, token, chat_id):
        """
        Initiates handler

        Parameters
        ----------
        api_id: int
            Telegram api id

        api_hash: str
            Telegram api hash

        token: str
            Bot's unique authentication

        chat_id: int
            The Telegram log chat id
        """
        super().__init__()
        self.bot_client = TelegramClient('log', api_id, api_hash).start(bot_token=token)
        self.bot_client.parse_mode = 'html'
        self.loop = asyncio.get_event_loop()
        self.chat_id = chat_id
        self.setFormatter(HtmlFormatter())

    async def log(self, message, traceback_text=None):
        """
        Logs a formatted record message to Telegram

        Parameters
        ----------
        message: str
            Formatted message

        traceback_text: str
            Traceback text if record is an error
        """
        if traceback_text is None:
            await self.bot_client.send_message(self.chat_id, message, link_preview=False)
        else:
            input_file = await self.bot_client.upload_file(BytesIO(traceback_text.encode()), file_name='traceback.log')
            await self.bot_client.send_file(self.chat_id, input_file, caption=message)

    def emit(self, record):
        """
        Emits a record.

        Parameters
        ----------
        record: LogRecord
            The log record
        """
        self.loop.run_until_complete(self.log(self.format(record), record.exc_text))


class HtmlFormatter(Formatter):
    """
    HTML formatter for telegram handler
    """
    fmt = '<b>{levelname}</b>\n{message}'

    def __init__(self):
        super().__init__(self.fmt, style='{')

    def format(self, record):
        """
        Format the specified record as text.

        Parameters
        ----------
        record: LogRecord
            The log record

        Returns
        -------
        result: str
            The formatted record
        """
        super().format(record)

        emoji_mapper = {
            logging.INFO: ':information:',
            logging.WARNING: ':warning:',
            logging.ERROR: ':no_entry:'
        }
        emoji = emojize(emoji_mapper[record.levelno], variant='emoji_type')
        record.levelname = f'{emoji} {record.levelname} {emoji}'

        return self.formatMessage(record)