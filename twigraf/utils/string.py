def isub(indices, replacement, string):
    """
    the string obtained by replacing the given indices of it by a replacement.

    Parameters
    ----------
    indices: list
        The 2D list of indices two be replaced

    replacement: callable or str
        The replacement could be callable or string

    string: str
        The given string

    Returns
    -------
    result: str
        The result
    """
    complement_flatten = sum(indices, [0]) + [len(string)]
    complement_indices = list(zip(complement_flatten, complement_flatten[1:]))[::2]
    replacements = [replacement(si, ei) for si, ei in indices] if callable(replacement) else [replacement
                                                                                              for _ in indices]
    result = '{}'.join([string[si:ei] for si, ei in complement_indices]).format(*replacements)
    return result
