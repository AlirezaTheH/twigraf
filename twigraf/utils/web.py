import logging
from urllib.parse import unquote

import requests
import bs4

logger = logging.getLogger('twigraf')


def get_final_url_title(url):
    """
    Redirects a url to destination url and gets the final url and title.

    Parameters
    ----------
    url: str
        The given url

    Returns
    -------
    result: tuple
        Tuple of url and title
    """
    response = requests.get(url)
    response.encoding = response.apparent_encoding
    soup = bs4.BeautifulSoup(response.text, 'lxml')
    url = unquote(response.url)
    title = None
    if soup.title is not None:
        title = soup.title.text
    return url, title


def download_file(url):
    """
    Downloads a file with a given url.

    Parameters
    ----------
    url: str
        File url

    Returns
    -------
    filename: str
        Name of downloaded file
    """
    logger.info(f'Downloading {url} started.')
    filename = url.split('/')[-1]
    with requests.get(url, stream=True) as r:
        with open(filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024*1024):
                if chunk:
                    f.write(chunk)
    logger.info(f'Downloading {url} done.')
    return filename


def get_file_size(url):
    """
    Gets a file size by its url

    Parameters
    ----------
    url: str
        The file url

    Returns
    -------
    result: int
        File size in bits
    """
    return int(requests.head(url).headers['Content-Length'])
