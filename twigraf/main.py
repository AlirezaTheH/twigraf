import argparse
import logging

import nest_asyncio
from environs import Env

from twigraf.twitter.bots import DirectMessageBot
from twigraf.db.base import SessionFactory
from twigraf.telegram.types import NetworkUsageLevel
from twigraf.utils.logging import TelegramHandler

nest_asyncio.apply()


def get_argparser():
    """
    Gets the command line argument parser.

    Returns
    -------
    parser: argparse.ArgumentParser
        The arg parser
    """
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    modes = ['debug', 'develop', 'release']
    parser.add_argument('-m',
                        '--mode',
                        metavar='mode',
                        default='debug',
                        choices=modes,
                        nargs='?',
                        help=f'The running mode. allowed modes: {", ".join(modes)}.')
    parser.add_argument('-n',
                        '--network-usage-level',
                        metavar='level',
                        default=NetworkUsageLevel.high,
                        choices=NetworkUsageLevel.enums,
                        nargs='?',
                        help=f'Network usage level. available levels: {", ".join(NetworkUsageLevel.enums)}.')
    parser.add_argument('-r',
                        '--recreate-tables',
                        action='store_true',
                        help='Recreate all database tables.')
    return parser


def main():
    parser = get_argparser()
    args = parser.parse_args()
    env = Env()
    env.read_env(f'environments/{args.mode}.env', recurse=False)
    session_factory = SessionFactory(env('database_engine'),
                                     env('database_user'),
                                     env('database_password'),
                                     env('database_host'),
                                     env('database_port'),
                                     env('database_name'),
                                     args.recreate_tables)
    session = session_factory.new_session()
    dm_bot = DirectMessageBot(env('twitter_platform_to_use'),
                              env('telegram_channel_username'),
                              env('telegram_media_channel_username'),
                              env('telegram_log_channel_username'),
                              env.int('telegram_api_id'),
                              env('telegram_api_hash'),
                              env('telegram_phone'),
                              env('telegram_password'),
                              env('telegram_bot_token'),
                              env.list('twitter_admins_screen_names'),
                              args.network_usage_level,
                              session)

    logger = logging.getLogger('twigraf')
    logger.setLevel(logging.INFO)
    handler = TelegramHandler(env.int('telegram_api_id'),
                              env('telegram_api_hash'),
                              env('telegram_bot_token'),
                              dm_bot.configuration.telegram_log_channel_id)
    logger.addHandler(handler)

    while True:
        try:
            logger.info('Listening ...')
            dm_bot.listen()

        except Exception as e:
            logger.exception('Something went wrong ...')
